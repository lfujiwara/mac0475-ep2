import axios from 'axios'

const http = axios.create({
  baseURL: process.env.AUTH_URL,
})

const register = ({ email, username, password }) =>
  http.post('/register', { email, username, password })

const login = ({ email, password }) => http.post('/login', { email, password })

export const auth = { register, login, http }
